const fs        = require('fs');
const path      = require('path');

let ct = {};
fs.readdirSync(__dirname+'/../controllers').filter(function(file) {
	return (file.indexOf(".") !== 0);
})
.forEach(function(file) {
	let fileName = file.substring(0, file.length - 3);
	ct[fileName] = require(path.join(__dirname+'/../controllers', file));
});

module.exports = ct;