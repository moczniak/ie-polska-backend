const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const configDB = require('./../config/config.js');


const sequelize = new Sequelize(configDB.db, configDB.username, configDB.password, {
	host: configDB.host,
	dialect: configDB.dialect,

	pool: {
		max: configDB.poolMax,
		min: configDB.poolMin,
		idle: configDB.Idle
	},
	logging: configDB.dbLoggin


});

let db = {};

fs.readdirSync(__dirname+'/../models').filter(function(file) {
	return (file.indexOf(".") !== 0);
})
.forEach(function(file) {
	var model = sequelize.import(path.join(__dirname+'/../models', file));
	db[model.name] = model;
});

Object.keys(db).forEach(function(modelName) {
	if ("associate" in db[modelName]) {
		db[modelName].associate(db);
	}
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
