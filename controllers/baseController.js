module.exports = class BaseController {

  constructor() {
    this._socket;
    this._respond;
    this._scServer;
    this._socketInputData;
    this._socketAuthData;
    this._socketID;
  }

  setSocket(socket) {
    this._socket = socket;
  }
  setRespond(respond) {
    this._respond = respond;
  }
  setScServer(scServer) {
    this._scServer = scServer;
  }
  setSocketInputData(socketInputData) {
    this._socketInputData = socketInputData;
  }
  setSocketAuthData(socketAuthData) {
    this._socketAuthData = socketAuthData;
  }
  setSocketID(socketID) {
    this._socketID = socketID;
  }

  sendSuccess(success) {
    if(typeof success === 'undefined') success = {};
    this._respond(JSON.stringify({ status:'ok', data: success }));
  }

  sendError(err) {
    this._respond(JSON.stringify({ status:'fail', data: err }));
  }

}
