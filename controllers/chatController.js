"use strict"
const generator = require('generator-async');
const validate = require('./../helpers/validation');
const helpers = require('./../helpers/helpers');
const models = require('./../loader').models;

const BaseController = require('./baseController');


module.exports = class ChatController extends BaseController {


	sendMessage() {
		generator.run((function*(){
			try{
				let username ='';
				let userInstance = yield* models.user.getUserById(this._socketAuthData.userID);
				let mssageInstance = yield* models.chat.sendMessage(userInstance.userID, this._socketInputData);
				let newMessage = {
					messageDate: mssageInstance.messageDate,
					user: {
						username: userInstance.username,
						userID: userInstance.userID
					},
					message: mssageInstance.message
				}
				this._scServer.exchange.publish('mainChat', newMessage);
				this.sendSuccess();
			}catch(err){ }
		}).bind(this));
	}

	getMainMessages() {
		generator.run((function*(){
			let limit = this._socketInputData > 0 ? this._socketInputData : 100;
			let messagesInstances = yield* models.chat.getMainMessages(limit);
			this.sendSuccess(messagesInstances);
		}).bind(this));
	}
}
