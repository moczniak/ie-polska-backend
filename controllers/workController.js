"use strict";
const generator = require('generator-async');
const models = require('./../loader').models;
const config = require('./../config/config.js');
const helpers = require('./../helpers/helpers');
const moment = require('moment');

const BaseController = require('./baseController');


module.exports = class WorkController extends BaseController {

			goToWork() {
				generator.run((function*(){
					try{
						let earned = 0;
						let	today = helpers.dateSundayToMonday(moment().day());
						let monthDay = moment().date();
						let workData = yield* models.userData.work(this._socketAuthData.userID);
						earned = workData.job.earned;
						this._scServer.exchange.publish('userNotify_' + this._socketID, workData.job);
						this._scServer.exchange.publish('userDetail_' + this._socketID, workData.details);
						yield* models.userJobLog.addOrUpdate(workData.instance);
			      this.sendSuccess({ earned, today, monthDay });
					}catch(err) {
						this.sendError(err.message);
					}
				}).bind(this));
			}



			getChartDataByPeriod() {
				if(this._socketInputData.period === 'week') {
					this._getChartDataByWeek();
				}else if (this._socketInputData.period === 'month') {
					this._getChartDataByMonth();
				}
			}

				_getChartDataByWeek() {
					generator.run((function*(){
						try{
							const currentSunday = moment().day(7).hour(0).minute(0).second(0).millisecond(0).utc();
							const currentMonday = moment().day(1).hour(0).minute(0).second(0).millisecond(0).utc();
							const lastMonday = moment().day(-6).hour(0).minute(0).second(0).millisecond(0).utc();
							const lastSunday = moment().day(0).hour(0).minute(0).second(0).millisecond(0).utc();
							let currentWeekData = yield* models.userJobLog.getDataFromWeek(this._socketAuthData.userID, currentMonday, currentSunday);
							let lastWeekData = yield* models.userJobLog.getDataFromWeek(this._socketAuthData.userID, lastMonday, lastSunday);
							this.sendSuccess({currentWeekData, lastWeekData});
						}catch(err) { }
					}).bind(this));
				}

				_getChartDataByMonth() {
					generator.run((function*(){
						try{
							const firstDayLastMonth = moment().subtract(1, 'months').date(1).hour(0).minute(0).second(0).millisecond(0).utc();
							const lastDayLastMonth = moment().subtract(1,'months').endOf('month').hour(0).minute(0).second(0).millisecond(0).utc();
							const lastDayCurrentMonth = moment().endOf('month').hour(0).minute(0).second(0).millisecond(0).utc();
							const firstDayCurrentMonth = moment().startOf('month').hour(0).minute(0).second(0).millisecond(0).utc();
							let currentMonthData = yield* models.userJobLog.getDataFromMonth(this._socketAuthData.userID, firstDayCurrentMonth, lastDayCurrentMonth);
							let lastMonthData = yield* models.userJobLog.getDataFromMonth(this._socketAuthData.userID, firstDayLastMonth, lastDayLastMonth);
							this.sendSuccess({currentMonthData, lastMonthData});
						}catch(err) { }
					}).bind(this));
				}


}
