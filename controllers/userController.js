"use strict"
const generator = require('generator-async');
const validate = require('./../helpers/validation');
const helpers = require('./../helpers/helpers');
const models = require('./../loader').models;

const BaseController = require('./baseController');

module.exports = class UserController extends BaseController{


	constructor(){
		super();
		this._registerFormValidation = {
			login: 'req|min:5|max:20|alnum',
			email: 'req|email',
			password: 'req|min:5|confirm:password===password2',
			password2: 'req:min:5',
			rules: 'boolean:true'
		}
		this._registerFormValidationMsg = {
			login: {
				req: 'Nazwa użytkownika wymagana',
				min: 'Nazwa użytkownika ma zawierać minimum 5 znaków',
				max: 'Nazwa użytkownika ma zawierać maksimum 20 znaków',
				alnum: 'Nazwa użytkownika może zawierać tylko znaki alfanumeryczne'
			},
			email: {
				req: 'Email jest wymagany',
				email: 'Podany adres email jest nieprawidłowy'
			},
			password: {
				req: 'Hasło jest wymagane',
				min: 'Hasło ma zawierać minimum 5znaków',
				confirm: 'Hasła nie zgadzają się'
			},
			password2: {
				req: 'Potwierdzenie hasła jest wymagane',
				min: 'Potwierdzenie hasła ma zawierać minimum 5 znaków'
			},
			rules: {
				boolean: 'Musisz zaakceptować zasady'
			}
		}


		this._loginFormValidation = {
			login: 'req',
			password: 'req'
		}
		this._loginFormValidationMsg = {
			login: {
				req: 'Nazwa użytkownika wymagana'
			},
			password: {
				req: 'Hasło jest wymagane'
			}
		}
		this._forgetFormPasswordValidation = {
			password: 'req|min:5|confirm:password===passwordConfirm'
		}
		this._forgetFormPasswordValidationMsg = {
			password: {
				req: 'Hasło jest wymagane',
				min: 'Hasło ma zawierać minimum 5znaków',
				confirm: 'Hasła nie zgadzają się'
			}
		}

	}

	static setIDifAuth(socket, userID) {
		if (helpers.isAuth(socket) === true) {
			models.user.setSocketID(socket.id, userID).then();
		}
	}


	create() {
		let Validator = new validate(this._registerFormValidation, this._registerFormValidationMsg);
		let isValidate = Validator.isValidate(this._socketInputData);
		if(isValidate === true){
			this._createIsValid();
		}else {
			this.sendError(isValidate);
		}
	}

		_createIsValid() {
			generator.run((function*() {
				try{
					let userInstance = yield* models.user.newUser(this._socketInputData);
					let activationInstance = yield* models.userActivation.createTokens(userInstance.userID);
					yield helpers.Mail({
						to: this._socketInputData.email,
						title: 'ie-polska.pl aktywacja konta',
						message: 'Kliknij na poniższy link by aktywować konto: <a href="http://localhost:3000/activate/'+activationInstance.token+'">Klik</a>'
					});
					this.sendSuccess({message: 'Sprawdź swojego emaila by aktywować konto'});
				}catch(err) {
					this.sendError(err.message);
				}
			}).bind(this));
		}


	activate() {
		generator.run((function*(){
			try{
				yield* models.user.activate(this._socketInputData);
				this.sendSuccess({message: 'Konto zostało aktywowane'});
			}catch(err) {
				this.sendError(err.message);
			}
		}).bind(this));
	}

	login() {
		let Validator = new validate(this._loginFormValidation, this._loginFormValidationMsg);
		let isValidate = Validator.isValidate(this._socketInputData);
		let autoLoginToken = '';
		if (isValidate === true) {
			this._loginIsValidate();
		}else {
			this.sendError(isValidate);
		}
	}

		_loginIsValidate() {
			generator.run((function*(){
				try{
					let instance = yield* models.user.login(this._socketInputData);
					this._socket.setAuthToken({userID: instance.userID, username: instance.username});
					yield* models.userLoginLog.addLogin(instance.userID, socket.remoteAddress);
					this.sendSuccess({autoLoginToken: instance.autoLoginToken});
				}catch(err){
					this.sendError(err.message);
				}
			}).bind(this));
		};



	autoLoginByToken() {
		generator.run((function*(){
			try{
				let instance = yield* models.user.loginByToken(this._socketInputData);
				this._socket.setAuthToken({userID: instance.userID, username: instance.username});
				this.sendSuccess();
			}catch(err){
				this.sendError(err.message);
			}
		}).bind(this));
	}


	remindMe() {
		let Validator = new validate({},{});
		if (Validator.isEmail(this._socketInputData)) {
			this._remindMeIsValidate();
		}else {
			this.sendError('Podany adre email jest nieprawidłowy');
		}
	}

		_remindMeIsValidate() {
			generator.run((function*(){
				try{
					let instance = yield* models.user.findByEmail(this._socketInputData);
					let userForgetPasswordInstance = yield* models.userForgetPassword.addToken(instance.userID);
					yield helpers.Mail({
						to: this._socketInputData,
						title: 'ie-polska.pl przypomnienie hasła',
						message: 'Kliknij na poniższy link by przypomnieć hasło: <a href="http://localhost:3000/remindMe/'+userForgetPasswordInstance.token+'">Klik</a>'
					});
					this.sendSuccess({message: 'Sprawdź swojego emaila by przypomnieć hasło'})
				}catch(err) {
					this.sendError(err.message);
				}
			}).bind(this));
		}

	forgetPassword() {
		let Validator = new validate(this._forgetFormPasswordValidation, this._forgetFormPasswordValidationMsg);
		let isValidate = Validator.isValidate(this._socketInputData);
		if(isValidate === true){
			this._forgetPasswordIsValidate();
		}else {
			this.sendError(isValidate);
		}
	}
		_forgetPasswordIsValidate() {
			generator.run((function*(){
				try{
					let instance = yield* models.user.forgetPassword(this._socketInputData.password, this._socketInputData.token);
					yield* models.userForgetPassword.deleteTokensByUserID(instance.userID);
					this.sendSuccess({message: 'Hasło zostało zmienione, możesz się zalogować'});
				}catch(err) {
					this.sendError(err.message);
				}
			}).bind(this));
		}

	logout() {
		generator.run((function*(){
			let userID = this._socketAuthData.userID;
			this._socket.deauthenticate();
			yield* models.user.logoutFull(userID);
			this.sendSuccess();
		}).bind(this));
	}

	checkIfAuth() {
		let status = false;
		if (helpers.isAuth(this._socket) === true) {
			status = true;
		}
		this._respond(JSON.stringify({status}));
	}


	getUserDetails(){
			generator.run((function*() {
				let result = yield* models.userData.updateStatistic(this._socketAuthData.userID);
				this.sendSuccess(result);
			}).bind(this));
	}

}
