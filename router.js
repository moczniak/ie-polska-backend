"use strict"
const helpers = require('./helpers/helpers');

const UserController = require('./controllers/userController');
const WorkController = require('./controllers/WorkController');
const ChatController = require('./controllers/ChatController');


module.exports = class Router{
	route(socket, scServer) {


		UserController.setIDifAuth(socket, socket.getAuthToken().userID);

		// MAIN PAGE
		socket.on('registerUser', (data, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(data);
			controller.create();
		});
		socket.on('activateUser', (token, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(token);
			controller.activate();
		});
		socket.on('loginUser', (data, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(data);
			controller.setSocket(socket);
			controller.login();
		});
		socket.on('autoLoginUser', (token, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(token);
			controller.setSocket(socket);
			controller.autoLoginByToken();
		});
		socket.on('remindMe', (email, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(email);
		});
		socket.on('changePassword', (data, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocketInputData(data);
			controller.forgetPassword();
		});
		socket.on('logoutUser', (emptyData, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocket(socket);
			controller.logout();
		});
		socket.on('checkIfAuth', (emptyData, respond) => {
			let controller = new UserController();
			controller.setRespond(respond);
			controller.setSocket(socket);
			controller.checkIfAuth();
		});



		// INGAME
		socket.on('getMainChatMessages', (limit, respond) => {
			let ct = new ChatController();
			ct.setRespond(respond);
			ct.setSocketInputData(limit);
			ct.getMainMessages();
		});

		// AUTH
		socket.on('chatSendMessage:Auth', (message, respond) => {
				let ct = new ChatController();
				ct.setRespond(respond);
				ct.setScServer(scServer);
				ct.setSocketInputData(message);
				ct.setSocketAuthData(socket.getAuthToken());
				ct.sendMessage();
		});
		socket.on('goToWork:Auth', (emptyData, respond) => {
			let ct = new WorkController();
			ct.setRespond(respond);
			ct.setSocketAuthData(socket.getAuthToken());
			ct.setScServer(scServer);
			ct.setSocketID(socket.id);
			ct.goToWork();
		});
		socket.on('getUserDetails:Auth', (emptyData, respond) => {
			let ct = new UserController();
			ct.setRespond(respond);
			ct.setSocketAuthData(socket.getAuthToken());
			ct.setSocketID(socket.id);
			ct.getUserDetails();
		});
		socket.on('getChartData:Auth', (data, respond) => {
			let ct = new WorkController();
			ct.setRespond(respond);
			ct.setSocketAuthData(socket.getAuthToken());
			ct.setSocketInputData(data);
			ct.getChartDataByPeriod();
		});





	}


}
