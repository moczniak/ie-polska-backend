const configDB = require('./../config/config.js');
const sp = require('sparkpost');
const SparkPost = new sp(configDB.sparkpost_api_key);

module.exports = {

  Mail: (email) => {
	return new Promise((resolve, reject) => {
		SparkPost.transmissions.send({
			transmissionBody: {
				content: {
					from: 'admin@ie-polska.pl',
					subject: email.title,
					html:'<html><body>'+email.message+'</body></html>'
				},
				recipients: [
					{address: email.to}
				]
				}
		}, function(err, res) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
  },

  randomString: (len, charSet) => {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomString = '';
    for (let i = 0; i < len; i++) {
    	let randomPoz = Math.floor(Math.random() * charSet.length);
    	randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
  },

  time: () => {
	   return Math.floor(new Date().getTime() / 1000);
  },

  isAuth: (socket) => {
	   return socket.authState === 'authenticated'? true : false;
  },

  dateSundayToMonday: (day) => {
    return day - (day == 0 ? -6:1)
  }


};
