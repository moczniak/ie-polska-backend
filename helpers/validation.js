module.exports = class Validation{	
	constructor(rules, msg){
		this.rules = rules;
		this.msg = msg;
	}
	
	isValidate(form){
		
		for (var key in this.rules) {
		   if (this.rules.hasOwnProperty(key)) {
			var rule = this.rules[key].split('|');
			for(var i =0; i < rule.length;++i) {
				var tmpSplit;
				if(rule[i] === 'req') {
					if(typeof form[key] === 'undefined' || form[key] === '') {
						return this.msg[key]['req'];
					}
				}else if(rule[i] === 'alnum') {
					var pattern = /^[a-z0-9]+$/i;
					if(typeof form[key] === 'undefined' || pattern.exec(form[key]) === null) {
						return this.msg[key]['alnum'];
					}
				}else if(rule[i] === 'email') {
					if(typeof form[key] === 'undefined' || this.isEmail(form[key]) === false) {
						return this.msg[key]['email'];
					}
				}else if(rule[i].indexOf('min:') > -1){
					tmpSplit = rule[i].split(':');
					if(typeof form[key] === 'undefined' || form[key].length < tmpSplit[1]) {
						return this.msg[key]['min'];
					}
				}else if(rule[i].indexOf('max:') > -1) {
					tmpSplit = rule[i].split(':');
					if(typeof form[key] === 'undefined' || form[key].length > tmpSplit[1]) {
						return this.msg[key]['max'];
					}
				}else if(rule[i].indexOf('confirm:') > -1) {
					tmpSplit = rule[i].split(':');
					var confirmKeys = tmpSplit[1].split('===');
					if(typeof form[confirmKeys[0]] === 'undefined' || typeof form[confirmKeys[1]] === 'undefined' || form[confirmKeys[0]] !== form[confirmKeys[1]] ) {
						return this.msg[key]['confirm'];
					}
				}else if(rule[i].indexOf('boolean:') > -1) {
					tmpSplit = rule[i].split(':');
					if(typeof form[key] === 'undefined' || form[key] !== this.stringToBoolean(tmpSplit[1])) {
						return this.msg[key]['boolean'];
					}
				}
			}
			
			
		   }
		}
		return true;
	}
	
	isEmail(email) {
		var pattern = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/; 
		return (pattern.exec(email) === null)? false : true;
	}
	
	stringToBoolean(string) {
		return string.toLowerCase() === 'true' ? true : false;
	}
	

}