module.exports = {
	loadMiddleware: (scServer) =>{
		scServer.addMiddleware(scServer.MIDDLEWARE_EMIT,(req, next) => {
			let authString = ':Auth'.toLowerCase();
			let event = req.event.toLowerCase();
			
			if (event.indexOf(authString) > -1) {
				let authToken = req.socket.getAuthToken();
				if( authToken !== null) {
					next();
				}else {
					
					next(JSON.stringify({
						status : 'unAuth'
					}));// brak autoryzacji
				}
			}else{
				next();
			}
		});
	}
	
}
