"use strict"
	const router = require('./router.js');
	const models = require('./loader').models;
	const middleware = require('./middleware');


module.exports.run = function (worker) {
	console.log('   >> Worker PID:', process.pid);
	const scServer = worker.scServer;
	const Router = new router();

	models.sequelize.sync().then(() => {
		middleware.loadMiddleware(scServer);
		scServer.on('connection', (socket) => {

			socket.setAuthToken({userID: 1, username: 'darthrivius'});
			
			Router.route(socket,scServer);

			socket.on('disconnect', () => {});

		});
	});
};
