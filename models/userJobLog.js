const helpers = require('./../helpers/helpers');


module.exports = function(sequelize, Sequelize) {

  const userJobLog = sequelize.define('userJobLog', {
    jLogID		    	: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    jobID           : Sequelize.INTEGER,
    jobPositionID   : Sequelize.INTEGER,
    userID          : Sequelize.INTEGER,
    workPtsUsed     : Sequelize.INTEGER,
    energyPtsUsed   : Sequelize.INTEGER,
    earned          : Sequelize.DECIMAL(10, 2),

  });

  userJobLog.associate = function(models) {
    this.belongsTo(models.user, {foreignKey: 'userID', as:'user'});
    this.belongsTo(models.job, {foreignKey: 'jobID', as:'job'});
    this.belongsTo(models.jobPosition, {foreignKey: 'jobPositionID', as:'jobPosition'});
  }


  userJobLog.addOrUpdate = function*(instance) {
      let date = new Date();
      const today00 = date.setHours(0, 0, 0, 0);
      const today24 = date.setHours(24, 0, 0, 0);
      let result = yield this.findOne({
        where: {
          userID         : instance.userID,
          jobID          : instance.jobID,
          jobPositionID  :  instance.jobPositionID,
          createdAt      : { $gt: today00, $lt: today24 }
        }
      });
      if (result === null) {
        yield this.create({
          userID         : instance.userID,
          jobID          : instance.jobID,
          jobPositionID  :  instance.jobPositionID,
          workPtsUsed   : instance.jobPosition.workPtsReq,
          earned        : instance.jobPosition.money,
          energyPtsUsed : instance.jobPosition.energyPtsReq
        });
        return null;
      }else {
        result.workPtsUsed += instance.jobPosition.workPtsReq;
        result.earned += instance.jobPosition.money;
        result.energyPtsUsed += instance.jobPosition.energyPtsReq;
        yield result.save();
        return null;
      }
  };



  userJobLog.getDataFromMonth = function*(userID, begin, end) {
      let result = yield this.findAll({where: { userID, createdAt: { $gte: begin, $lte: end } }});
      let daysEarned = new Array(31).fill(0);
      result.map(res => {
        daysEarned[res.createdAt.getDate()] += res.earned;
      });
      return daysEarned;
  }

  userJobLog.getDataFromWeek = function*(userID, begin, end) {
      let result = yield this.findAll({ where: { userID, createdAt: { $gte: begin, $lte: end }}});
      let daysEarned = new Array(7).fill(0);

      result.filter(res => res.createdAt >= begin && res.createdAt <= end ).map(res => {
        daysEarned[helpers.dateSundayToMonday(res.createdAt.getDay())] += res.earned;
      });
      return daysEarned;
  }



  return userJobLog;
}
