const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {


	const chat = sequelize.define('chat', {
		chatID			: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		userID			: Sequelize.INTEGER,
		message			: Sequelize.STRING,
		messageDate	: Sequelize.INTEGER
	});

	chat.associate = function(models) {
		this.belongsTo(models.user, {foreignKey: 'userID', as:'user'});
	}

	chat.sendMessage = function*(userID, txt) {
			const timeStamp = helpers.time();
			let message = new Object();
			message.userID = userID;
			message.message = txt;
			message.messageDate = timeStamp;
			return yield this.create(message);
	};

	chat.getMainMessages = function*(limit) {
			let result = yield this.findAll({ attributes: ['message', 'messageDate'], limit: limit, 	order: [['messageDate', 'DESC']],
									 include: [ { model: sequelize.models.user, as: 'user', required: false, attributes:['username', 'userID'] } ] });
			return result.reverse();
	};

	return chat;
}
