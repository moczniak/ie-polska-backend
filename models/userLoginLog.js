const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {

	const userLoginLog = sequelize.define('userLoginLog', {
		logID	: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		userID	: Sequelize.INTEGER,
		ip		: Sequelize.STRING
	});

	userLoginLog.associate = function(models) {
		this.belongsTo(models.user, {foreignKey: 'userID', as:'user'});
	};

	userLoginLog.addLogin = function*(userID, IP) {
			let tmpIP = IP.split('::ffff:');
			return yield this.create({ userID, ip: tmpIP[1] });
	};

	return userLoginLog;

}
