const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {


	const jobPosition = sequelize.define('jobPosition', {
		jobPositionID		: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		name						: Sequelize.STRING,
		money						:	Sequelize.INTEGER,
		requirements		: Sequelize.STRING,
		workPtsReq			:	Sequelize.INTEGER,
		energyPtsReq		:	Sequelize.INTEGER,
		jobID						: Sequelize.INTEGER
	});


	return jobPosition;

}
