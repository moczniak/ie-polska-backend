const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {

	const userActivation = sequelize.define('userActivation', {
		activationID	: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		userID			: Sequelize.INTEGER,
		token			: Sequelize.STRING
	});


	userActivation.associate = function(models) {
		this.belongsTo(models.user, {foreignKey: 'userID', as:'user'});
	};


	userActivation.createTokens = function*(userID) {
			let token = helpers.randomString(50);
			return yield this.create({ userID, token });
	};

	return userActivation;

}
