const helpers = require('./../helpers/helpers');
const config = require('./../config/config');

module.exports = function(sequelize,Sequelize) {

	const userData = sequelize.define('userData', {
		udataID					: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		userID					: Sequelize.INTEGER,
		money						: Sequelize.DECIMAL(10, 2),
		workPts					: Sequelize.INTEGER,
		workPtsMax			: Sequelize.INTEGER,
		workPtsStamp		:	Sequelize.INTEGER,
		actionPts				: Sequelize.INTEGER,
		actionPtsMax		: Sequelize.INTEGER,
		actionPtsStamp	:	Sequelize.INTEGER,
		energyPts				: Sequelize.INTEGER,
		energyPtsMax		: Sequelize.INTEGER,
		energyPtsStamp	:	Sequelize.INTEGER,
		healthyPts			: Sequelize.INTEGER,
		healthyPtsMax		: Sequelize.INTEGER,
		healthyPtsStamp	:	Sequelize.INTEGER,
		jobID						: Sequelize.INTEGER,
		jobPositionID		:	Sequelize.INTEGER,
	 });

	 	userData.associate = function() {
			this.belongsTo(sequelize.models.user, {foreignKey: 'userID', as:'user'});
			this.hasOne(sequelize.models.job, {foreignKey: 'jobID', as:'job'});
			this.hasOne(sequelize.models.jobPosition, { foreignKey: 'jobPositionID', as: 'jobPosition' });
		}

		userData.prototype.calcTimeLeftToNextPts = function(field, regenerationMinute) {
			return ( regenerationMinute * 60 ) - (helpers.time() - this[field + 'PtsStamp']);
		};

		userData.prototype.updateStat = function(field, regenerationMinute) {
				let timeLeftToNext = 0;
				if (this[field + 'Pts'] < this[field + 'PtsMax']) {
					const timeStamp = helpers.time();
					const stampLeft = timeStamp - this[field + 'PtsStamp'];
					const regTime = regenerationMinute * 60;
					if (stampLeft >= regTime) {
						const div = parseInt(stampLeft / regTime, 10);
						if (div > 0) {
							if ( ( div + this[field + 'Pts'] ) >= this[field + 'PtsMax']) {
								this[field + 'Pts'] = this[field + 'PtsMax'];
								this[field + 'PtsStamp'] = timeStamp;
							}else {
								this[field + 'Pts'] += div;
								this[field + 'PtsStamp'] += (div * regTime);
							}
						}
					}
				}
				if (this[field + 'Pts'] < this[field + 'PtsMax']) {
					timeLeftToNext = this.calcTimeLeftToNextPts(field, regenerationMinute);
				}
				return timeLeftToNext;
		}

		userData.prototype.getAndUpdateUserDetail = function() {
			let details = {};
			details.workTimeLeft = this.updateStat('work', config.workPtsRegenerationMinute);
			details.energyTimeLeft = this.updateStat('energy', config.energyPtsRegenerationMinute);
			details.actionTimeLeft = this.updateStat('action', config.actionPtsRegenerationMinute);
				Object.assign(details, {
					workPts					: this.workPts,
					workPtsMax			: this.workPtsMax,
					energyPts				:	this.energyPts,
					energyPtsMax		:	this.energyPtsMax,
					actionPts 			:	this.actionPts,
					actionPtsMax		:	this.actionPtsMax,
					money						:	this.money,
					healthyPts			:	this.healthyPts,
					healthyPtsMax		:	this.healthyPtsMax
				});
				return details;
		};

		userData.updateStatistic = function*(userID) {
			 let instance = yield this.findOne({ where: { userID }, include: [{ model: sequelize.models.jobPosition, as: 'jobPosition', required: false }]});
			 let details = instance.getAndUpdateUserDetail();
			 yield instance.save();
			 return details;
	 }


	 userData.work = function*(userID) {
			 let instance = yield this.findOne({ where: { userID: userID }, include: [ { model: sequelize.models.jobPosition, as: 'jobPosition', required: false }]});
			 let details = instance.getAndUpdateUserDetail();
			 if (instance.workPts >= instance.jobPosition.workPtsReq) {
				if (instance.energyPts >= instance.jobPosition.energyPtsReq) {
					const time = helpers.time();
					const job = {
						earned: instance.jobPosition.money,
						position: instance.jobPosition.name,
						type: 'job'
					}
					instance.money += job.earned;
					if (instance.workPts === instance.workPtsMax) {
						instance.workPtsStamp = time;
					}
					if (instance.energyPts === instance.energyPtsMax) {
						instance.energyPtsStamp = time;
					}
					instance.workPts -= instance.jobPosition.workPtsReq;
					instance.energyPts -= instance.jobPosition.energyPtsReq;
					details = instance.getAndUpdateUserDetail();
					let savedInstance = yield instance.save();
					return {instance: savedInstance, details, job};
				}else {
					throw new Error('Nie masz energii by dalej pracować');
				}
			}else {
				throw new Error('Nie masz tylu punktów by pracować dalej');
			}
	 }

	return userData;
}
