const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {

	const job = sequelize.define('job', {
		jobID				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		name				: Sequelize.STRING,
		jobType			: Sequelize.STRING
	});

	job.associate = function(models) {
		this.hasMany(models.jobPosition, {foreignKey: 'jobID', as:'jobPosition'});
	}

	return job;
}
