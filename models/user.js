const passwordHash = require('password-hash');
const helpers = require('./../helpers/helpers');


module.exports = (sequelize,Sequelize) => {


	const user = sequelize.define('user', {
		userID								: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		username							: {type: Sequelize.STRING, unique: {msg: 'Podana nazwa użytkownika jest zajęta'}},
		password							: Sequelize.STRING,
		email									: {type: Sequelize.STRING, unique: {msg: 'Podany adres email jest zajęty'}},
		active								: Sequelize.BOOLEAN,
		autoLoginToken				: Sequelize.STRING,
		tokenExpirationStamp	: Sequelize.INTEGER,
		socketID							:	Sequelize.STRING
	});

	user.associate = function (models) {
		this.hasOne(models.userActivation, {foreignKey: 'userID', as: 'userActivation'});
		this.hasOne(models.userForgetPassword, {foreignKey: 'userID', as: 'userForgetPassword'});
		this.hasMany(models.chat, {foreignKey: 'userID', as: 'chat'});
		this.hasMany(models.userJobLog, {foreignKey: 'userID', as: 'userJobLog'})
	}


	user.setSocketID = function(socketID, userID) {
			return this.update({ socketID }, {where:{ userID }});
	};

	user.findByEmail = function*(email) {
			let instance = yield this.findOne({where:{ email }});
			if (instance !== null) {
				return instance;
			}else {
				throw new Error('Nie ma takiego adresu email');
			}
	};

	user.newUser = function*(data) {
		try{
			let password = passwordHash.generate(data.password);
			let user = {
				username: data.login,
				password: password,
				email: data.email,
				userData: {
					money					: 1000,
					workPts				: 10,
					workPtsMax		: 10,
					dirtyPts			: 10,
					dirtyPtsMax		: 10,
					energyPts			: 100,
					energyPtsMax	: 100,
					HealthyPts		: 100,
					HealtyPtsMax	: 100
					}
				};
			return yield this.create(user);
		}catch(err) {
			console.log(err);
			//err.errors[0].message
			throw new Error('user.newUser ERROR');
		}
	};

	user.activate = function*(token) {
			let instance = yield this.findOne({include: [{ model: sequelize.models.userActivation, as:'userActivation', where:{token} } ]})
			if (instance !== null) {
				instance.active = 1;
				yield instance.save();
				return yield instance.userActivation.destroy()
			}else {
				throw new Error('Podany token aktywacyjny nie istnieje');
			}
	};

	user.login = function*(form) {
			let instance = yield this.findOne({where:{ username: form.login }, attributes:['userID','username','password','active','autoLoginToken'] })
			if (instance !== null) {
				if (passwordHash.verify(form.password, instance.password) === true) {
					if (form.rememberme === true) {
						instance.autoLoginToken = helpers.randomString(50);
						instance.tokenExpirationStamp = helpers.time()+config.autoLoginTokenExpire;
						return yield instance.save();
					}else {
						resolve(instance);
					}
				}else {
					throw new Error('Podane hasło jest niepoprawne');
				}
			}else {
				throw new Error('Nazwa użytkownika nie istnieje');
			}
	};

	user.loginByToken = function*(autoLoginToken) {
			let instance = yield this.findOne({ where: { autoLoginToken }, attributes:['userID','username','tokenExpirationStamp','autoLoginToken'] });
			if (instance !== null) {
				if (instance.tokenExpirationStamp >= helpers.time()) {
					return instance;
				}else {
					instance.tokenExpirationStamp = 0;
					instance.autoLoginToken = '';
					return yield instance.save();
				}
			}else {
				throw new Error('Twoje autologowanie wygasło');
			}
	};

	user.logoutFull = function*(userID) {
			return yield this.update({	autoLoginToken: '', tokenExpirationStamp:0 }, { where: { userID } });
	};

	user.forgetPassword = function*(password, token) {
			let instance = yield this.findOne({ include: [{ model: sequelize.models.userForgetPassword, as:'userForgetPassword', where:{ token } }]});
			if (instance !== null) {
				let newPassword = passwordHash.generate(password);
				instance.password = newPassword;
				return yield instance.save();
			}else {
				throw new Error('Podany token nie istnieje');
			}
	};

	user.getUserById = function*(userID) {
			let instance = yield this.findOne({ where: { userID } });
			if (instance !== null) {
				return instance;
			}else {
				throw new Error('Nie ma takiego użytkownika');
			}
	};

return user;

}
