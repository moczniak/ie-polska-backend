const helpers = require('./../helpers/helpers');


module.exports = function(sequelize,Sequelize) {

	const userForgetPassword = sequelize.define('userForgetPassword', {
		forgetID	: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		userID		: Sequelize.INTEGER,
		token		: Sequelize.STRING
	});

	userForgetPassword.associate = function(models) {
		this.belongsTo(models.user, {foreignKey: 'userID', as:'user'});
	};

	userForgetPassword.addToken = function*(userID) {
			let token = helpers.randomString(50);
			return yield this.create({ userID, token });
	};

	userForgetPassword.deleteTokensByUserID = function*(userID) {
			return yield this.destroy({ where: { userID } });
	};

	return userForgetPassword;


}
